<?php

// Connect to the database using MySQLi
$host = 'localhost';
$username = 'root';
$password = 'santan1233212';
$database = 'p8_exercise_backend';


$conn = new mysqli($host, $username, $password, $database);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


// Insert data into the employee table
$sql = "INSERT INTO employee (first_name, middle_name, last_name, birthday, address)
VALUES ('Jane', 'Marie', 'Doe', '1990-01-01', '456 Elm Street')";
if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}


// retrieve the first name, last name, and birthday of all employees in the table
$sql =  "SELECT first_name, last_name, birthday FROM employee";
$result = $conn->query($sql);
echo "<br>";
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
    }
} else {
    echo "0 results";
}


// retrieve the number of employees whose last name starts with the letter 'S'
$sql = "SELECT * FROM employee WHERE last_name LIKE 'D%'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $numberOfEmployees = $result->num_rows;
    echo "Number of employees with last name starting with 'S': " . $numberOfEmployees . "<br>";
} else {
    echo "0 results";
}


//  retrieve the first name, last name, and address of the employee with the highest ID number
$sql = "SELECT first_name, last_name, address FROM employee ORDER BY id DESC LIMIT 1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    echo "First Name: " . $row["first_name"] . " - Last Name: " . $row["last_name"] . " - Address: " . $row["address"] . "<br>";
} else {
    echo "0 results";
}

// Update data in the employee table
$sql =  "UPDATE employee SET address='123 Main Street' WHERE first_name='John'";
if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully";
} else {
    echo "Error updating record: " . $conn->error;
}

// Delete data from the employee table
$sql = "DELETE FROM employee WHERE last_name LIKE 'D%'";
echo "<br>";
if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}
$conn->close();


?>
